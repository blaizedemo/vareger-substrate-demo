mod inner;
mod tests;

use crate::models::{
	enums::ResourceType,
	scalar::DefaultResult,
	storage::{Ingredient, IngredientResource, Resource, Stamping, StampingsWithAmounts},
};

use codec::{Codec, Decode, Encode};
use rstd::{collections::btree_map::BTreeMap as Map, num::Wrapping, prelude::*};
use sr_primitives::{
	traits::{Member, SaturatedConversion, Saturating, SignedExtension, SimpleArithmetic, Zero},
	transaction_validity::{TransactionPriority, TransactionValidity, TransactionValidityError, ValidTransaction},
	weights::DispatchInfo,
};
use support::{decl_event, decl_module, decl_storage, dispatch::Result, Parameter};
use system::{ensure_signed, ensure_root, IsDeadAccount};

pub trait Trait {
	type Event: From<Event<Self>> + Into<<Self as system::Trait>::Event>;
	type String: Parameter + From<Vec<u8>> + Into<Vec<u8>> + Default + Clone;
	type Balance: Parameter + Member + SimpleArithmetic + Codec + Default + Copy + From<u128>;
}

decl_event!(
	pub enum Event<T>
	where
		AccountId = <T as system::Trait>::AccountId,
	{
		ResourceSupply(ResourceType),
		AmountMint(AccountId, u128),
		Transfer(AccountId, AccountId, u128),
	}
);

const MIN_AMOUNT_TO_MINT: u128 = 1_000;
const MIN_AMOUNT_TO_TRANSFER: u128 = 1;

decl_storage! {
	trait Store for Module<T: Trait> as example {
		pub ResourceRates get(resource_rates) config(): Map<ResourceType, u128>;

		pub ResourcesCount get(resources_count): u64;
		pub Resources get(resources): map u64 => Resource<T::String>;
		pub Ingredients get(ingredients): map ResourceType => Option<Ingredient>;

		pub StampingsCount get(stampings_count): u64 = 0;
		pub Stampings get(stampings): map u64 => Stamping;

		pub Balances get(balances): map T::AccountId => u128 = 0;
		pub BalanceStampings get(balance_stampings): map T::AccountId => Map<u64, u128> = Map::new();
	}
}

decl_module! {
	pub struct Module<T: Trait> for enum Call where origin: T::Origin {
		fn deposit_event() = default;

		pub fn supply(origin, identification_code: T::String, resource_type: ResourceType, weight: u128) -> Result {
			ensure_root(origin)?;
			Self::_supply(identification_code, resource_type, weight)?;
			Ok(())
		}

		pub fn mint(origin, amount: u128) -> Result {
			let who = ensure_signed(origin)?;
			Self::_mint(who.clone(), amount)?;
			Ok(())
		}

		pub fn transfer(origin, to: T::AccountId, amount: u128) -> Result {
			let who = ensure_signed(origin)?;
			Self::_transfer(who.clone(), to.clone(), amount.clone())?;
			Ok(())
		}
	}
}

#[derive(Encode, Decode, Clone, Eq, PartialEq)]
pub struct TakeFees<T: Trait>(#[codec(compact)] T::Balance);

impl<T: Trait> TakeFees<T> {
	pub fn from(fee: T::Balance) -> Self {
		Self(fee)
	}

	fn compute_fee(len: usize, info: DispatchInfo, tip: T::Balance) -> T::Balance {
		let len_fee = if info.pay_length_fee() {
			let len = T::Balance::from(len as u32);
			let per_byte = T::Balance::from(0_u32);
			per_byte.saturating_mul(len)
		} else {
			Zero::zero()
		};
		len_fee.saturating_add(tip)
	}
}

#[cfg(feature = "std")]
impl<T: Trait> rstd::fmt::Debug for TakeFees<T> {
	fn fmt(&self, f: &mut rstd::fmt::Formatter) -> rstd::fmt::Result {
		self.0.fmt(f)
	}
}

impl<T: Trait> SignedExtension for TakeFees<T> {
	type AccountId = T::AccountId;
	type Call = T::Call;
	type AdditionalSigned = ();
	type Pre = ();

	fn additional_signed(&self) -> rstd::result::Result<(), TransactionValidityError> {
		Ok(())
	}

	fn validate(&self, _: &Self::AccountId, _: &Self::Call, info: DispatchInfo, len: usize) -> TransactionValidity {
		let fee = Self::compute_fee(len, info, self.0);
		let mut r = ValidTransaction::default();
		r.priority = fee.saturated_into::<TransactionPriority>();
		Ok(r)
	}
}

impl<T: Trait> IsDeadAccount<T::AccountId> for Module<T> {
	fn is_dead_account(who: &T::AccountId) -> bool {
		Self::balances(who).is_zero()
	}
}
