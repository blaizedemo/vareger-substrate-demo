use super::*;
#[cfg(feature = "std")]
use crate::models::{
	enums::ResourceType,
	storage::{Ingredient, Stamping},
};

use rstd::collections::btree_map::BTreeMap as Map;
use rstd::prelude::*;
use support::ensure;

impl<T: Trait> Module<T> {
	pub fn next_ingredient(resource_id: u64, weight: u128, resource_type: ResourceType) -> Ingredient {
		let res = IngredientResource { resource_id, weight };
		match Self::ingredients(resource_type) {
			Some(mut ing) => {
				ing.resources.push(res);
				ing.available_weight += weight;
				ing
			}
			None => Ingredient {
				resources: vec![res],
				available_weight: weight,
				resource_type,
			},
		}
	}

	pub fn make_sure_ingredients_enough(amount: u128) -> Option<Vec<Ingredient>> {
		let mut ingredients = vec![];
		let resource_rates = Self::resource_rates();

		for (resource_type, resource_rate) in resource_rates {
			if let Some(ing) = Self::ingredients(resource_type) {
				let amount_can_be_minted = ing.available_weight / resource_rate;
				if amount_can_be_minted < amount {
					return None;
				}
				ingredients.push(ing);
			}
		}
		Some(ingredients)
	}

	pub fn last_res_total(
		ingredients: Vec<Ingredient>,
		resource_rates: Map<ResourceType, u128>,
		amount: u128,
	) -> DefaultResult<u128> {
		let mut min_total_amount = 2_u128.pow(60);
		for ing in ingredients {
			let resource_rate = resource_rates
				.get(&ing.resource_type)
				.ok_or("FATAL: Failed to get rates")?;

			let resource_amount_needed = amount * resource_rate;
			let mut resource_amount = 0;

			for res in ing.resources {
				if resource_amount < resource_amount_needed {
					let diff = resource_amount_needed - resource_amount;

					let new_min_total_amount = res.weight / resource_rate;
					if min_total_amount > new_min_total_amount {
						min_total_amount = new_min_total_amount;
					}

					let weight = if diff < res.weight { diff } else { res.weight };
					resource_amount += weight;
				}
			}
		}
		Ok(min_total_amount)
	}

	pub fn get_stampings_to_create(
		ingredients: &mut Vec<Ingredient>,
		amount: u128,
	) -> DefaultResult<(StampingsWithAmounts, StampingsWithAmounts)> {
		let mut resources_to_stampings = Map::new();
		let mut new_stampings = vec![];
		let mut updated_stampings = vec![];
		let resource_rates = Self::resource_rates();
		let last_res_total = Self::last_res_total(ingredients.clone(), resource_rates.clone(), amount)?;

		let stampings_count = Self::stampings_count();
		let mut minted_amount = 0;
		if stampings_count > 0 {
			let mut stamp = Self::stampings(stampings_count);
			if stamp.minted_amount < stamp.total_amount {
				minted_amount = Self::update_existing_stamping(&mut stamp, ingredients, amount)?;
				updated_stampings.push((stamp, minted_amount));
			}
		}

		if minted_amount < amount {
			for ing in ingredients {
				let resource_rate = resource_rates
					.get(&ing.resource_type)
					.ok_or("FATAL: Failed to get rates")?;
				let resource_amount_needed = (amount - minted_amount) * resource_rate;
				let best_resources =
					Self::get_best_resources_to_mint_from(ing, resource_amount_needed, last_res_total)?;
				resources_to_stampings.insert(ing.resource_type, best_resources);
				ing.available_weight -= resource_amount_needed;
			}
			new_stampings = Self::get_stampings_from_best_resources(resources_to_stampings)?;
		}
		Ok((new_stampings, updated_stampings))
	}

	pub fn update_existing_stamping(
		existing_stamping: &mut Stamping,
		ingredients: &mut Vec<Ingredient>,
		amount: u128,
	) -> DefaultResult<u128> {
		let resource_rates = Self::resource_rates();
		let mut minted_amount = 0;

		for ing in ingredients {
			let resource_rate = resource_rates
				.get(&ing.resource_type)
				.ok_or("FATAL: Failed to get rates")?;
			let mut stamp_res = existing_stamping
				.resources
				.get_mut(&ing.resource_type)
				.ok_or("FATAL: Failed to get stamping resource")?;
			let (ing_id, ing_res) = ing
				.resources
				.iter_mut()
				.enumerate()
				.find(|(_, x)| x.resource_id == stamp_res.resource_id)
				.ok_or("FATAL: Failed to get ingredient resource")?;

			let resource_amount_to_mint = amount * resource_rate;
			let all_res_amount_in_stamp = existing_stamping.total_amount * resource_rate;
			let rem_res_amount = all_res_amount_in_stamp - stamp_res.weight;

			let amount_to_mint = if rem_res_amount >= resource_amount_to_mint {
				resource_amount_to_mint
			} else {
				rem_res_amount
			};

			ing.available_weight -= amount_to_mint;
			ing_res.weight -= amount_to_mint;
			stamp_res.weight += amount_to_mint;
			if ing_res.weight == 0 {
				ing.resources.remove(ing_id);
			}
			let new_minted_amount = amount_to_mint / resource_rate;
			if minted_amount != 0 && new_minted_amount != minted_amount {
				return Err("FATAL: Minted amount difference");
			}
			minted_amount = new_minted_amount;
		}
		existing_stamping.minted_amount += amount;
		Ok(minted_amount)
	}

	pub fn get_best_resources_to_mint_from(
		ing: &mut Ingredient,
		resource_amount_needed: u128,
		last_res_total: u128,
	) -> DefaultResult<Vec<(IngredientResource, u128)>> {
		let resource_rates = Self::resource_rates();
		let mut resources = vec![];
		let mut resource_amount = 0;

		for resource in ing.resources.iter_mut() {
			if resource_amount < resource_amount_needed {
				let resource_rate = resource_rates
					.get(&ing.resource_type)
					.ok_or("FATAL: Failed to get rates")?;

				let diff = resource_amount_needed - resource_amount;
				let total_weight;

				let weight = if diff < resource.weight {
					// Todo: Fix overflow error
					total_weight = Wrapping(last_res_total * resource_rate - diff);
					resource.weight -= diff;
					diff
				} else {
					total_weight = Wrapping(0);
					resource.weight
				};
				resources.push((
					IngredientResource {
						resource_id: resource.resource_id,
						weight,
					},
					total_weight.0,
				));
				resource_amount += weight;
			} else {
				break;
			}
		}
		if resource_amount != resource_amount_needed {
			return Err("Not enough resources");
		}
		ing.resources.retain(|res| {
			let mut keep = true;
			for (res_to_rem, weight) in resources.clone() {
				if res.resource_id == res_to_rem.resource_id && weight == 0 {
					keep = false;
				}
			}
			keep
		});
		Ok(resources)
	}

	pub fn get_stampings_from_best_resources(
		resources_to_stampings: Map<ResourceType, Vec<(IngredientResource, u128)>>,
	) -> DefaultResult<StampingsWithAmounts> {
		let mut stampings = vec![];
		let resource_rates = Self::resource_rates();

		let next_stamping_id = match Self::stampings_count() {
			num if num > 0 => num + 1,
			_ => 1,
		};

		let first_res = resources_to_stampings
			.get(&ResourceType::Gold)
			.ok_or("FATAL: Can not get resource")?;
		for enumerator in 0..first_res.len() {
			let mut resources: Map<ResourceType, IngredientResource> = Map::new();
			let mut minted_amount = 0;
			let mut total_amount = 0;

			for (resource_type, ing_res) in resources_to_stampings.clone() {
				let resource_rate = resource_rates.get(&resource_type).ok_or("FATAL: Failed to get rate")?;
				let (ing, weight) = ing_res[enumerator].clone();

				minted_amount = ing.weight / resource_rate;

				let new_total_amount = (ing.weight + weight) / resource_rate;
				if total_amount != 0 && new_total_amount != total_amount {
					// FIXME: Amount 4001950000000000 not working
					return Err("FATAL: Total amount difference");
				}
				total_amount = new_total_amount;

				resources.insert(resource_type, ing);
			}

			let stamp = Stamping {
				id: next_stamping_id + (enumerator as u64),
				resources,
				minted_amount,
				total_amount,
			};
			stampings.push((stamp, minted_amount));
		}
		Ok(stampings)
	}

	pub fn transfer_stampings(
		sender_stampings: &mut Map<u64, u128>,
		receiver_stampings: &mut Map<u64, u128>,
		amount: u128,
	) -> DefaultResult<()> {
		let mut stamps_to_remove = vec![];

		let mut sum: u128 = 0;
		for (stamping_id, sender_stamping_amount) in sender_stampings.iter_mut() {
			if sum >= amount {
				break;
			}

			let change = if *sender_stamping_amount > amount && sum == 0 {
				sum += amount;
				*sender_stamping_amount -= amount;
				amount
			} else if *sender_stamping_amount <= amount && sum == 0 {
				sum += *sender_stamping_amount;
				stamps_to_remove.push(stamping_id.clone());
				*sender_stamping_amount
			} else if *sender_stamping_amount > (amount - sum) {
				let diff = amount - sum;
				sum += diff;
				*sender_stamping_amount -= diff;
				diff
			} else if *sender_stamping_amount == (amount - sum) {
				sum += *sender_stamping_amount;
				stamps_to_remove.push(stamping_id.clone());
				*sender_stamping_amount
			} else {
				sum += *sender_stamping_amount;
				stamps_to_remove.push(stamping_id.clone());
				*sender_stamping_amount
			};

			match receiver_stampings.get_mut(stamping_id) {
				Some(recst_amount) => *recst_amount += change,
				None => {
					receiver_stampings.insert(stamping_id.clone(), change);
				}
			}
		}
		ensure!(sum <= amount, "FATAL: Transfer over amount");

		for stamp_id in stamps_to_remove {
			sender_stampings.remove(&stamp_id);
		}
		Ok(())
	}

	pub fn mint_amount(who: T::AccountId, amount: u128) -> DefaultResult<()> {
		let mut ingredients =
			Self::make_sure_ingredients_enough(amount).ok_or("Not enough ingredients to mint that amount of coins")?;
		ensure!(
			ingredients.len() == ResourceType::get_types().len(),
			"Not enough ingredients"
		);

		let (new_stampings, updated_stampings) = Self::get_stampings_to_create(&mut ingredients, amount)?;
		ensure!(
			new_stampings.len() + updated_stampings.len() > 0,
			"FATAL: Failed to modify stampings"
		);
		ensure!(!ingredients.is_empty(), "FATAL: Failed to modify ingredients");

		let stampings = [&new_stampings[..], &updated_stampings[..]].concat();
		for (stamping, _) in stampings.clone() {
			Stampings::insert(stamping.id, stamping);
		}
		StampingsCount::mutate(|x| *x += new_stampings.len() as u64);

		for ingredient in ingredients {
			Ingredients::insert(ingredient.resource_type, ingredient);
		}
		<Balances<T>>::mutate(who.clone(), |x| *x += amount);
		<BalanceStampings<T>>::mutate(who.clone(), |x| {
			for (stamping, minted_amount) in stampings {
				match x.get_mut(&stamping.id) {
					Some(ex_st) => {
						*ex_st += minted_amount;
					}
					None => {
						x.insert(stamping.id, minted_amount);
					}
				}
			}
		});

		Ok(())
	}

	// pub fn store_mint_info

	pub fn _supply(identification_code: T::String, resource_type: ResourceType, weight: u128) -> DefaultResult<()> {
		let resource_rates = Self::resource_rates();
		let resource_rate = resource_rates.get(&resource_type).ok_or("Failed to get rate")?;
		assert!(weight % resource_rate == 0, "Weight should be even to resource rate");

		let next_resource_id = match Self::resources_count() {
			num if num > 0 => num + 1,
			_ => 1,
		};

		let ingredient = Self::next_ingredient(next_resource_id, weight, resource_type);
		let resource = Resource {
			identification_code,
			resource_type,
			weight,
		};

		<Resources<T>>::insert(next_resource_id, resource.clone());
		Ingredients::insert(resource_type, ingredient);
		ResourcesCount::mutate(|x| *x = next_resource_id);

		Self::deposit_event(RawEvent::ResourceSupply(resource_type));
		Ok(())
	}

	pub fn _mint(who: T::AccountId, amount: u128) -> DefaultResult<()> {
		ensure!(amount >= Self::min_amount_to_mint(), "Too small amount");
		Self::mint_amount(who.clone(), amount)?;
		Self::deposit_event(RawEvent::AmountMint(who, amount));
		Ok(())
	}

	pub fn _transfer(who: T::AccountId, to: T::AccountId, amount: u128) -> DefaultResult<()> {
		ensure!(amount >= Self::min_amount_to_transfer(), "Too small amount");
		ensure!(who != to, "The receiver should be different from the sender");

		let balance = Self::balances(who.clone());
		ensure!(amount <= balance, "Insufficient balance");

		let mut sender_stampings = Self::balance_stampings(who.clone());
		let mut receiver_stampings = Self::balance_stampings(to.clone());
		Self::transfer_stampings(&mut sender_stampings, &mut receiver_stampings, amount)?;

		<BalanceStampings<T>>::insert(who.clone(), sender_stampings);
		<Balances<T>>::mutate(who.clone(), |x| *x -= amount);

		<BalanceStampings<T>>::insert(to.clone(), receiver_stampings);
		<Balances<T>>::mutate(to.clone(), |x| *x += amount);

		Self::deposit_event(RawEvent::Transfer(who, to, amount));
		Ok(())
	}

	// FIXME: Add these through tests
	#[cfg(feature = "std")]
	pub fn add_rates(rates: Map<ResourceType, u128>) {
		ResourceRates::put(rates.clone())
	}
}
