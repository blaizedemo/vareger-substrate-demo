#![cfg(test)]

use super::*;
use crate::models::{
    enums::ResourceType,
    storage::{Ingredient, IngredientResource, Resource},
};

use primitives::H256;
use rstd::collections::btree_map::BTreeMap as Map;
use rstd::iter::FromIterator;
use sr_primitives::{
    testing::Header,
    traits::{BlakeTwo256, IdentityLookup},
    weights::Weight,
    Perbill,
};
use support::{assert_noop, assert_ok, impl_outer_origin, parameter_types};
use system::ensure_signed;

impl_outer_origin! {
    pub enum Origin for Test {}
}

// For testing the module, we construct most of a mock runtime. This means
// first constructing a configuration type (`Test`) which `impl`s each of the
// configuration traits of modules we want to use.
#[derive(Clone, Eq, PartialEq)]
pub struct Test;

parameter_types! {
    pub const BlockHashCount: u64 = 250;
    pub const MaximumBlockWeight: Weight = 1024;
    pub const MaximumBlockLength: u32 = 2 * 1024;
    pub const AvailableBlockRatio: Perbill = Perbill::from_percent(75);
}

impl system::Trait for Test {
    type Origin = Origin;
    type Call = ();
    type Index = u64;
    type BlockNumber = u64;
    type Hash = H256;
    type Hashing = BlakeTwo256;
    type AccountId = u64;
    type Lookup = IdentityLookup<Self::AccountId>;
    type Header = Header;
    type WeightMultiplierUpdate = ();
    type Event = ();
    type BlockHashCount = BlockHashCount;
    type MaximumBlockWeight = MaximumBlockWeight;
    type MaximumBlockLength = MaximumBlockLength;
    type AvailableBlockRatio = AvailableBlockRatio;
    type Version = ();
}

impl Trait for Test {
    type Event = ();
    type String = Vec<u8>;
    type Balance = u128;
}

type Token = Module<Test>;

// This function basically just builds a genesis storage key/value store according to
// our desired mockup.
fn new_test_ext() -> runtime_io::TestExternalities {
    system::GenesisConfig::default().build_storage::<Test>().unwrap().into()
}

pub fn default_resource_rates() -> Map<ResourceType, u128> {
    let rates = vec![
        (ResourceType::Platinum, 10000),
        (ResourceType::Gold, 10000),
    ];
    Map::from_iter(rates.into_iter())
}

fn add_resource_rates() {
    assert_eq!(Token::resource_rates().len(), 0);

    let rates = default_resource_rates();
    assert_eq!(Token::add_rates(rates.clone()), ());

    assert_eq!(Token::resource_rates(), rates);
}

fn add_defaults() {
    add_resource_rates();
}

fn call_supply(res: Resource<<Test as Trait>::String>) -> u128 {
    let mut count = Token::resources_count();
    let old_ingredient = Token::ingredients(res.resource_type);

    assert_ok!(Token::supply(
        Origin::ROOT,
        res.identification_code.clone(),
        res.resource_type.clone(),
        res.weight.clone()
    ));
    count += 1;

    assert_eq!(Token::resources_count(), count);
    assert_eq!(Token::resources(count), res.clone());

    let ing = Token::ingredients(res.resource_type);
    assert_eq!(ing.is_some(), true);
    if old_ingredient.is_some() {
        assert_eq!(
            old_ingredient.unwrap().available_weight + res.weight,
            ing.unwrap().available_weight
        );
    }
    u128::from(count)
}

fn default_resources(count: u128) -> Vec<(u128, Resource<<Test as Trait>::String>)> {
    let mut resources = vec![];
    let mut enumerator: u128 = 0;

    for (res_type, amount) in default_resource_rates() {
        enumerator += 1;
        let res = Resource {
            identification_code: "code".as_bytes().to_vec().into(),
            resource_type: res_type,
            weight: amount * count,
        };
        resources.push((enumerator, res));
    }
    resources
}

fn default_resources_with_inc(count: u128) -> Vec<(u128, Resource<<Test as Trait>::String>)> {
    let mut resources = vec![];
    let mut enumerator = 0;

    // Todo: Check resources

    for (res_type, amount) in default_resource_rates() {
        let en_count = count + (enumerator * 100);
        enumerator += 1;
        let res = Resource {
            identification_code: "code".as_bytes().to_vec().into(),
            resource_type: res_type,
            weight: amount * en_count,
        };
        resources.push((enumerator, res));
    }
    resources
}

fn transfer_to_somebody(sender_origin: Origin, receiver_origin: Origin, amount: u128) {
    let sender = ensure_signed(sender_origin.clone()).unwrap();
    let receiver = ensure_signed(receiver_origin.clone()).unwrap();
    let sender_initial_balance = Token::balances(sender);
    let receiver_initial_balance = Token::balances(receiver);

    let sender_balances_stampings = Token::balance_stampings(sender.clone());
    let mut sum: u128 = 0;
    println!("{:?}", sender_balances_stampings);
    for (_, stamping_amount) in sender_balances_stampings {
        println!("stamping_amount: {:?}", stamping_amount);
        sum += stamping_amount;
    }
    assert_eq!(sum, sender_initial_balance);

    assert_ok!(Token::transfer(sender_origin.clone(), receiver.clone(), amount));

    assert_eq!(Token::balances(sender.clone()), sender_initial_balance - amount);
    assert_eq!(Token::balances(receiver.clone()), receiver_initial_balance + amount);

    let sender_balances_stampings = Token::balance_stampings(sender.clone());
    let mut sum: u128 = 0;
    println!("{:?}", sender_balances_stampings);
    for (_, stamping_amount) in sender_balances_stampings {
        println!("stamping_amount: {:?}", stamping_amount);
        sum += stamping_amount;
    }
    assert_eq!(sum, sender_initial_balance - amount);

    let receiver_balances_stampings = Token::balance_stampings(receiver.clone());
    let mut sum: u128 = 0;
    for (_, stamping_amount) in receiver_balances_stampings {
        sum += stamping_amount;
    }
    assert_eq!(sum, receiver_initial_balance + amount);
}

fn mint_some_amount(origin: Origin, amount: u128) {
    let who = ensure_signed(origin.clone()).unwrap();
    let initial_balance = Token::balances(who);

    // Todo: Store count and check it's increment after mint
    // Token::stampings_count()

    let balance_stampings = Token::balance_stampings(who.clone());
    let mut sum: u128 = 0;
    for (_, value) in balance_stampings {
        sum += value;
    }
    assert_eq!(sum, initial_balance);

    assert_ok!(Token::mint(origin, amount));

    let new_amount_in_example = initial_balance + amount;
    assert_eq!(Token::balances(who), new_amount_in_example);

    let balance_stampings = Token::balance_stampings(who.clone());
    let mut sum: u128 = 0;
    for (_, value) in balance_stampings {
        sum += value;
    }
    assert_eq!(sum, new_amount_in_example);
}

#[test]
fn test_resource_rates() {
    new_test_ext().execute_with(|| {
        add_defaults();
    })
}

#[test]
fn test_next_ingredient() {
    new_test_ext().execute_with(|| {
        let ing_res = IngredientResource {
            resource_id: 1,
            weight: 10_u128,
        };
        let ing = Ingredient {
            resources: vec![ing_res.clone()],
            available_weight: ing_res.weight,
            resource_type: ResourceType::Gold,
        };
        assert_eq!(
            Token::next_ingredient(ing_res.resource_id, ing_res.weight, ing.resource_type),
            ing
        );
    })
}

#[test]
fn test_supply() {
    new_test_ext().execute_with(|| {
        add_defaults();
        let res: Resource<<Test as Trait>::String> = Resource {
            identification_code: "code".as_bytes().to_vec().into(),
            resource_type: ResourceType::Gold,
            weight: 10_u128,
        };
        call_supply(res.clone());
        call_supply(res.clone());
        assert_eq!(Token::resources_count(), 2);
    })
}

#[test]
fn test_make_sure_ingredients_enough() {
    new_test_ext().execute_with(|| {
        add_defaults();
        for (id, res) in default_resources(10_u128) {
            assert_eq!(call_supply(res), id);
        }
        assert_eq!(Token::ingredients(ResourceType::Gold).is_some(), true);
        let ingredients = Token::make_sure_ingredients_enough(10_u128);
        assert_eq!(ingredients.is_some(), true);
        assert_eq!(ingredients.unwrap().len(), 9);
    })
}

#[test]
fn test_last_res_total() {
    new_test_ext().execute_with(|| {
        let amount = 10_u128;

        add_defaults();
        for (id, res) in default_resources_with_inc(amount) {
            assert_eq!(call_supply(res), id);
        }
        assert_eq!(Token::ingredients(ResourceType::Gold).is_some(), true);
        let ingredients = Token::make_sure_ingredients_enough(amount);
        assert_eq!(ingredients.is_some(), true);
        let ings = ingredients.unwrap();
        assert_eq!(ings.len(), 9);

        let resource_rates = Token::resource_rates();
        let total = Token::last_res_total(ings, resource_rates, 9_u128);
        assert_eq!(total.is_ok(), true);
        let total = total.unwrap();
        assert_eq!(total, amount);
    })
}

#[test]
fn test_get_best_resources_to_mint_from() {
    new_test_ext().execute_with(|| {
        add_defaults();

        let mut ing = Ingredient {
            resources: vec![
                IngredientResource {
                    resource_id: 1,
                    weight: 10_000_000_u128,
                },
                IngredientResource {
                    resource_id: 2,
                    weight: 5_000_000_u128,
                },
            ],
            available_weight: 15_000_000_u128,
            resource_type: ResourceType::Gold,
        };

        let amount = 13_000_000_u128;
        let res = Token::get_best_resources_to_mint_from(&mut ing, amount, 500_u128);
        assert_eq!(res.unwrap()[1].1, ing.available_weight - amount);
        assert_eq!(ing.resources.len(), 1);

        let amount = 2_000_000_u128;
        assert_eq!(
            Token::get_best_resources_to_mint_from(&mut ing, amount, 500_u128).is_ok(),
            true
        );
        assert_eq!(ing.resources.len(), 0);

        let amount = 1_u128;
        assert_noop!(
            Token::get_best_resources_to_mint_from(&mut ing, amount, 500_u128),
            "Not enough resources"
        );
    })
}

#[test]
fn test_get_stampings_to_create() {
    new_test_ext().execute_with(|| {
        add_defaults();
        for (_, res) in default_resources(10_u128) {
            call_supply(res.clone());
            call_supply(res.clone());
        }
        assert_eq!(Token::ingredients(ResourceType::Gold).is_some(), true);
        let ingredients = Token::make_sure_ingredients_enough(10_u128);
        assert_eq!(ingredients.is_some(), true);
        let mut ings = ingredients.unwrap();
        assert_eq!(ings.len(), 9);

        let amount = 20_u128;
        assert_eq!(Token::get_stampings_to_create(&mut ings, amount).is_ok(), true);

        let amount = 10_000_u128;
        assert_noop!(
            Token::get_stampings_to_create(&mut ings, amount),
            "Not enough resources"
        );
    })
}

#[test]
fn test_mint() {
    new_test_ext().execute_with(|| {
        add_defaults();
        for (_, res) in default_resources(10_u128) {
            call_supply(res.clone());
            call_supply(res.clone());
        }
        assert_eq!(Token::ingredients(ResourceType::Gold).is_some(), true);
        let ingredients = Token::make_sure_ingredients_enough(10_u128);
        assert_eq!(ingredients.is_some(), true);
        assert_eq!(ingredients.unwrap().len(), 9);

        mint_some_amount(Origin::signed(1), 10_010_000_u128);
        assert_eq!(Token::stampings_count(), 2);
        assert_eq!(Token::stampings(2).minted_amount, 10_u128);
    })
}

#[test]
fn test_double_mint() {
    new_test_ext().execute_with(|| {
        add_defaults();
        for (_, res) in default_resources(10_u128) {
            call_supply(res.clone());
            call_supply(res.clone());
        }
        let amount = 10_020_000_u128;
        mint_some_amount(Origin::signed(1), amount);
        assert_eq!(Token::stampings_count(), 2);
        assert_eq!(Token::stampings(2).minted_amount, 20_000_u128);

        let amount = 9_980_000_u128;
        mint_some_amount(Origin::signed(1), amount);
        assert_eq!(Token::stampings_count(), 2);
        assert_eq!(Token::stampings(2).minted_amount, 10_u128);
    })
}

#[test]
fn test_mint_custom_amount() {
    new_test_ext().execute_with(|| {
        add_defaults();
        for (_, res) in default_resources_with_inc(1_u128) {
            call_supply(res.clone());
        }
        mint_some_amount(Origin::signed(1), 120_000_u128);
        assert_eq!(Token::stampings_count(), 1);
        assert_eq!(Token::stampings(1).minted_amount, 120_000_u128);

        mint_some_amount(Origin::signed(1), 880_000_u128);
        assert_eq!(Token::stampings_count(), 1);
        assert_eq!(Token::stampings(1).minted_amount, 1_u128);
    })
}

#[test]
fn test_transfer() {
    new_test_ext().execute_with(|| {
        let origin = Origin::signed(1);
        let receiver_origin = Origin::signed(2);
        let amount_in_example = 100_u128 * 10;

        add_defaults();
        for (_, res) in default_resources(10_u128) {
            call_supply(res.clone());
            call_supply(res.clone());
        }

        mint_some_amount(origin.clone(), 10_020_000_u128);
        transfer_to_somebody(origin.clone(), receiver_origin.clone(), amount_in_example);
    })
}

#[test]
fn test_remint() {
    new_test_ext().execute_with(|| {
        let origin = Origin::signed(1);
        let receciver_origin = Origin::signed(2);

        add_defaults();
        for (_, res) in default_resources_with_inc(10_u128) {
            call_supply(res.clone());
        }
        mint_some_amount(origin.clone(), 10_u128);
        transfer_to_somebody(origin.clone(), receciver_origin.clone(), 5_u128);

        assert_noop!(
            Token::mint(origin.clone(), 5_u128),
            "Not enough ingredients to mint that amount of coins"
        );

        mint_some_amount(origin.clone(), 4_u128);
        assert_noop!(
            Token::mint(origin.clone(), 2_u128),
            "Not enough ingredients to mint that amount of coins"
        );

        for (_, res) in default_resources_with_inc(10_000_u128) {
            call_supply(res.clone());
        }
        mint_some_amount(origin.clone(), 1_010_u128);
    })
}

#[test]
fn test_multi_mint_transfer() {
    new_test_ext().execute_with(|| {
        let origin = Origin::signed(1);
        let receiver_origin = Origin::signed(2);

        add_defaults();
        for (_, res) in default_resources_with_inc(10_u128) {
            call_supply(res.clone());
        }
        mint_some_amount(origin.clone(), 10_u128);
        transfer_to_somebody(origin.clone(), receiver_origin.clone(), 5_u128);

        assert_noop!(
            Token::mint(origin.clone(), 5_u128),
            "Not enough ingredients to mint that amount of coins"
        );

        // Some code...

        for (_, res) in default_resources_with_inc(10_000_u128) {
            call_supply(res.clone());
        }
        mint_some_amount(origin.clone(), 100_000_u128);

        transfer_to_somebody(origin.clone(), receiver_origin.clone(), 100_000_u128);
        mint_some_amount(origin.clone(), 910_000_u128);
        transfer_to_somebody(receiver_origin.clone(), origin.clone(), 100_000_u128);

        // Some code...

        transfer_to_somebody(origin.clone(), receiver_origin.clone(), 1_u128);
        mint_some_amount(receiver_origin.clone(), 1_u128);
        mint_some_amount(receiver_origin.clone(), 1_u128);

        // Some code...
        for (_, res) in default_resources_with_inc(100_000_u128) {
            call_supply(res.clone());
        }
        mint_some_amount(origin.clone(), 140_000_u128);
        transfer_to_somebody(origin.clone(), receiver_origin.clone(), 1_u128);
    })
}

#[test]
fn test_multi_mrt() {
    new_test_ext().execute_with(|| {
        let origin = Origin::signed(1);
        let receiver_origin = Origin::signed(2);

        add_defaults();
        for (_, res) in default_resources_with_inc(10_u128) {
            call_supply(res.clone());
        }
        mint_some_amount(origin.clone(), 10_u128);

        // Some code...
    })
}

#[test]
fn test_mint_with_stamping() {
    new_test_ext().execute_with(|| {
        let origin = Origin::signed(1);

        add_defaults();
        for (_, res) in default_resources_with_inc(10_u128) {
            call_supply(res.clone());
        }
        mint_some_amount(origin.clone(), 10_u128);

        for (_, res) in default_resources_with_inc(10_u128) {
            call_supply(res.clone());
        }
        mint_some_amount(origin.clone(), 10_u128);
        for (_, res) in default_resources_with_inc(10_u128) {
            call_supply(res.clone());
        }
        mint_some_amount(origin.clone(), 10_u128);
        // Some code...
        mint_some_amount(origin.clone(), 15_u128);
    })
}
