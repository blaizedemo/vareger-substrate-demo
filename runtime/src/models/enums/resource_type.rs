use rstd::prelude::*;
use codec::{Decode, Encode};

#[cfg_attr(feature = "std", derive(Serialize, Deserialize, Debug))]
#[derive(Clone, Copy, Encode, Decode, PartialEq, PartialOrd, Ord, Eq)]
pub enum ResourceType {
	Platinum,
	Gold,
}

impl Default for ResourceType {
	fn default() -> Self {
		Self::Platinum
	}
}

impl ResourceType {
	pub fn get_types() -> Vec<Self> {
		vec![
			Self::Platinum,
			Self::Gold,
		]
	}
}
