use rstd::result::Result;

pub type DefaultResult<T> = Result<T, &'static str>;
