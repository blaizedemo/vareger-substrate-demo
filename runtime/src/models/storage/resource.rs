use crate::models::enums::ResourceType;

use codec::{Decode, Encode};

#[cfg_attr(feature = "std", derive(Serialize, Deserialize, Debug))]
#[derive(Encode, Decode, Default, Clone, PartialEq)]
pub struct Resource<String: Clone> {
	pub identification_code: String,
	pub resource_type: ResourceType,
	pub weight: u128,
}
