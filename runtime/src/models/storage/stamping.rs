use crate::models::{enums::ResourceType, storage::IngredientResource};

use rstd::prelude::*;
use codec::{Decode, Encode};
use rstd::collections::btree_map::BTreeMap as Map;

#[cfg_attr(feature = "std", derive(Serialize, Deserialize, Debug))]
#[derive(Encode, Decode, Default, Clone, PartialEq)]
pub struct Stamping {
	pub id: u64,
	pub resources: Map<ResourceType, IngredientResource>,
	pub minted_amount: u128,
	pub total_amount: u128,
}

pub type StampingsWithAmounts = Vec<(Stamping, u128)>;
