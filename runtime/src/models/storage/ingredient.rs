use crate::models::enums::ResourceType;

use rstd::prelude::*;
use codec::{Decode, Encode};

#[cfg_attr(feature = "std", derive(Serialize, Deserialize, Debug))]
#[derive(Encode, Decode, Default, Clone, PartialEq)]
pub struct IngredientResource {
	pub resource_id: u64,
	pub weight: u128,
}

#[cfg_attr(feature = "std", derive(Serialize, Deserialize, Debug))]
#[derive(Encode, Decode, Default, Clone, PartialEq)]
pub struct Ingredient {
	pub resources: Vec<IngredientResource>,
	pub available_weight: u128,
	pub resource_type: ResourceType,
}
